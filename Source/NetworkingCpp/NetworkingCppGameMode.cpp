// Copyright Epic Games, Inc. All Rights Reserved.

#include "NetworkingCppGameMode.h"
#include "NetworkingCppCharacter.h"
#include "UObject/ConstructorHelpers.h"

ANetworkingCppGameMode::ANetworkingCppGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
