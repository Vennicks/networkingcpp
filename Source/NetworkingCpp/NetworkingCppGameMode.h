// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NetworkingCppGameMode.generated.h"

UCLASS(minimalapi)
class ANetworkingCppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANetworkingCppGameMode();
};



