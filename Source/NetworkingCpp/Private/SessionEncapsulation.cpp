// Fill out your copyright notice in the Description page of Project Settings.


#include "SessionEncapsulation.h"
#include "Interfaces/OnlineSessionInterface.h"

USessionEncapsulation::USessionEncapsulation(FOnlineSessionSearchResult ToEncapsulate)
    : JoinSessionCompleteDelegate(FOnJoinSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnJoinSessionCompleted))
{
    sessionResult = ToEncapsulate;
    session = ToEncapsulate.Session;
};


int USessionEncapsulation::GetPing()
{
    if (sessionResult.PingInMs >= MAX_QUERY_PING)
        return -1;
    return sessionResult.PingInMs;
}

void USessionEncapsulation::JoinSession()
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnJoinGameSessionCompleteEvent.Broadcast(CastJoinResultToTrueResult(EOnJoinSessionCompleteResult::UnknownError));
			return;
		}

		JoinSessionCompleteDelegateHandle = SessionInterface->AddOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegate);

		const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
		if (!SessionInterface->JoinSession(*localPlayer->GetPreferredUniqueNetId(), NAME_GameSession, sessionResult))
		{
			SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegateHandle);
			OnJoinGameSessionCompleteEvent.Broadcast(CastJoinResultToTrueResult(EOnJoinSessionCompleteResult::UnknownError));
		}
	}
}

void USessionEncapsulation::OnJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
			SessionInterface->ClearOnJoinSessionCompleteDelegate_Handle(JoinSessionCompleteDelegateHandle);
	}
	OnJoinGameSessionCompleteEvent.Broadcast(CastJoinResultToTrueResult(Result));
}

EOnJoinSessionStatus::type USessionEncapsulation::CastJoinResultToTrueResult(EOnJoinSessionCompleteResult::Type type)
{
	switch (type)
	{
	case EOnJoinSessionCompleteResult::Success:
			return EOnJoinSessionStatus::Success;
	case EOnJoinSessionCompleteResult::SessionIsFull:
			return EOnJoinSessionStatus::SessionIsFull;
	case EOnJoinSessionCompleteResult::SessionDoesNotExist:
			return EOnJoinSessionStatus::SessionDoesNotExist;
	case EOnJoinSessionCompleteResult::CouldNotRetrieveAddress:
			return EOnJoinSessionStatus::CouldNotRetrieveAddress;
	case EOnJoinSessionCompleteResult::AlreadyInSession:
			return EOnJoinSessionStatus::AlreadyInSession;
	case EOnJoinSessionCompleteResult::UnknownError:
			return EOnJoinSessionStatus::UnknownError;
		default:
			return EOnJoinSessionStatus::UnknownError;
	}
}

bool USessionEncapsulation::TryTravelToCurrentSession()
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
			return false;

		FString connectString;
		if (!SessionInterface->GetResolvedConnectString(NAME_GameSession, connectString))
			return false;

		APlayerController* playerController = GetWorld()->GetFirstPlayerController();
		playerController->ClientTravel(connectString, TRAVEL_Absolute);
		return true;
	}
	return false;
}
