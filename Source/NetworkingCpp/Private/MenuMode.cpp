// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuMode.h"
#include "Online/OnlineSessionNames.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"


AMenuMode::AMenuMode()
	: CreateSessionCompleteDelegate(FOnCreateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnCreateSessionCompleted)),
	DestroySessionCompleteDelegate(FOnDestroySessionCompleteDelegate::CreateUObject(this, &ThisClass::OnDestroySessionCompleted)),
	UpdateSessionCompleteDelegate(FOnUpdateSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnUpdateSessionCompleted)),
	StartSessionCompleteDelegate(FOnStartSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnStartSessionCompleted)),
	EndSessionCompleteDelegate(FOnEndSessionCompleteDelegate::CreateUObject(this, &ThisClass::OnEndSessionCompleted)),
	FindSessionsCompleteDelegate(FOnFindSessionsCompleteDelegate::CreateUObject(this, &ThisClass::OnFindSessionsCompleted))
{
}

#pragma region Create Session
void AMenuMode::CreateSession(FSessionDefinition NewSession)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnCreateSessionCompleteEvent.Broadcast(false);
			return;
		}

		LastSessionSettings = MakeShareable(new FOnlineSessionSettings());
		LastSessionSettings->NumPrivateConnections = 0;
		//???? LastSessionSettings->NumPublicConnections = NumPublicConnections;
		LastSessionSettings->bAllowInvites = NewSession.AllowInvites;
		LastSessionSettings->bAllowJoinInProgress = NewSession.JoinInGame;
		LastSessionSettings->bIsDedicated = !NewSession.AllowPlayerHost;
		LastSessionSettings->bIsLANMatch = NewSession.UseLan;
		LastSessionSettings->bShouldAdvertise = NewSession.Advertise;

		LastSessionSettings->bUsesPresence = true;
		LastSessionSettings->bAllowJoinViaPresence = true;
		LastSessionSettings->bAllowJoinViaPresenceFriendsOnly = true;

		LastSessionSettings->Set(KEY_SESSION_NAME, NewSession.SessionName, EOnlineDataAdvertisementType::ViaOnlineService);

		CreateSessionCompleteDelegateHandle = SessionInterface->AddOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegate);

		const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
		if (!SessionInterface->CreateSession(*localPlayer->GetPreferredUniqueNetId(), NAME_GameSession, *LastSessionSettings))
		{
			SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegateHandle);

			OnCreateSessionCompleteEvent.Broadcast(false);
		}
	}
}

void AMenuMode::OnCreateSessionCompleted(FName SessionName, bool Successful)
{
	if (IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
		{
			SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(CreateSessionCompleteDelegateHandle);
			return;
		}
	}

	OnCreateSessionCompleteEvent.Broadcast(Successful);
}
#pragma endregion

#pragma region Destroy Session
void AMenuMode::DestroySession()
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnDestroySessionCompleteEvent.Broadcast(false);
			return;
		}

		DestroySessionCompleteDelegateHandle =
			SessionInterface->AddOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegate);

		if (!SessionInterface->DestroySession(NAME_GameSession))
		{
			SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegateHandle);

			OnDestroySessionCompleteEvent.Broadcast(false);
		}
	}
}

void AMenuMode::OnDestroySessionCompleted(FName SessionName, bool Successful)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
		{
			SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(DestroySessionCompleteDelegateHandle);
		}

		OnDestroySessionCompleteEvent.Broadcast(Successful);
	}
}
#pragma endregion


#pragma region Update Session
void AMenuMode::UpdateSession(FSessionDefinition NewDefinition)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnUpdateSessionCompleteEvent.Broadcast(false);
			return;
		}

		TSharedPtr<FOnlineSessionSettings> updatedSessionSettings = MakeShareable(new FOnlineSessionSettings(*LastSessionSettings));

		//???? LastSessionSettings->NumPublicConnections = NumPublicConnections;
		updatedSessionSettings->bAllowInvites = NewDefinition.AllowInvites;
		updatedSessionSettings->bAllowJoinInProgress = NewDefinition.JoinInGame;
		updatedSessionSettings->bIsDedicated = !NewDefinition.AllowPlayerHost;
		updatedSessionSettings->bIsLANMatch = NewDefinition.UseLan;
		updatedSessionSettings->bShouldAdvertise = NewDefinition.Advertise;

		updatedSessionSettings->bUsesPresence = true;
		updatedSessionSettings->bAllowJoinViaPresence = true;
		updatedSessionSettings->bAllowJoinViaPresenceFriendsOnly = true;

		LastSessionSettings->Set(KEY_SESSION_NAME, NewDefinition.SessionName, EOnlineDataAdvertisementType::ViaOnlineService);

		UpdateSessionCompleteDelegateHandle = SessionInterface->AddOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegate);

		if (!SessionInterface->UpdateSession(NAME_GameSession, *updatedSessionSettings))
		{
			SessionInterface->ClearOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegateHandle);
			OnUpdateSessionCompleteEvent.Broadcast(false);
		}
		else
			LastSessionSettings = updatedSessionSettings;
	}
}

void AMenuMode::OnUpdateSessionCompleted(FName SessionName, bool Successful)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
			SessionInterface->ClearOnUpdateSessionCompleteDelegate_Handle(UpdateSessionCompleteDelegateHandle);
	}
	OnUpdateSessionCompleteEvent.Broadcast(Successful);
}
#pragma endregion

#pragma region Start Session
void AMenuMode::StartSession()
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnStartSessionCompleteEvent.Broadcast(false);
			return;
		}

		StartSessionCompleteDelegateHandle = SessionInterface->AddOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegate);

		if (!SessionInterface->StartSession(NAME_GameSession))
		{
			SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegateHandle);
			OnStartSessionCompleteEvent.Broadcast(false);
		}
	}
}

void AMenuMode::OnStartSessionCompleted(FName SessionName, bool Successful)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		if (const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface(); SessionInterface.IsValid())
			SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(StartSessionCompleteDelegateHandle);
	}

	OnStartSessionCompleteEvent.Broadcast(Successful);
}
#pragma endregion

#pragma region End Session
void AMenuMode::EndSession()
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
		{
			OnEndSessionCompleteEvent.Broadcast(false);
			return;
		}

		EndSessionCompleteDelegateHandle = SessionInterface->AddOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegate);

		if (!SessionInterface->EndSession(NAME_GameSession))
		{
			SessionInterface->ClearOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegateHandle);
			OnEndSessionCompleteEvent.Broadcast(false);
		}
	}
}

void AMenuMode::OnEndSessionCompleted(FName SessionName, bool Successful)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
	{
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
			SessionInterface->ClearOnEndSessionCompleteDelegate_Handle(EndSessionCompleteDelegateHandle);
	}

	OnEndSessionCompleteEvent.Broadcast(Successful);
}
#pragma endregion

#pragma region Find Session
void AMenuMode::FindSessions(int32 MaxSearchResults, bool IsLANQuery)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
    {
	    const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (!SessionInterface.IsValid())
        {
			OnFindSessionsCompleteEvent.Broadcast(TArray<USessionEncapsulation*>(), false);
			return;
		}

		FindSessionsCompleteDelegateHandle = SessionInterface->AddOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegate);

		LastSessionSearch = MakeShareable(new FOnlineSessionSearch());
		LastSessionSearch->MaxSearchResults = MaxSearchResults;
		LastSessionSearch->bIsLanQuery = IsLANQuery;
		LastSessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

		const ULocalPlayer* localPlayer = GetWorld()->GetFirstLocalPlayerFromController();
		if (!SessionInterface->FindSessions(*localPlayer->GetPreferredUniqueNetId(), LastSessionSearch.ToSharedRef()))
		{
	        SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegateHandle);
			OnFindSessionsCompleteEvent.Broadcast(TArray<USessionEncapsulation *>(), false);
		}
    }
}
void AMenuMode::OnFindSessionsCompleted(bool Successful)
{
	if (const IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get())
    {
		const IOnlineSessionPtr SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface)
			SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(FindSessionsCompleteDelegateHandle);
		
		if (LastSessionSearch->SearchResults.Num() <= 0)
		{
			OnFindSessionsCompleteEvent.Broadcast(TArray<USessionEncapsulation*>(), Successful);
			return;
		}

		TArray<USessionEncapsulation*> result;
        for(const auto session : LastSessionSearch->SearchResults)
        {
			USessionEncapsulation EncapsulatedSession(session);
            result.Add(&EncapsulatedSession);
        }

		OnFindSessionsCompleteEvent.Broadcast(result, Successful);
    }
}
#pragma endregion