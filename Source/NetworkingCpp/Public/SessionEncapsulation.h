#pragma once

#include "CoreMinimal.h"
#include "OnlineSessionSettings.h"
#include "OnlineSubsystem.h"
#include "Engine/ObjectReferencer.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "SessionEncapsulation.generated.h"

UENUM(BlueprintType)
namespace EOnJoinSessionStatus
{
    enum type
    {
        Success,
        SessionIsFull,
        SessionDoesNotExist,
        CouldNotRetrieveAddress,
        AlreadyInSession,
        UnknownError
    };
}


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnJoinSessionComplete, EOnJoinSessionStatus::type, Result);

UCLASS(BlueprintType)
class NETWORKINGCPP_API USessionEncapsulation : public UGameInstanceSubsystem
{
    GENERATED_BODY()

public:
    USessionEncapsulation(FOnlineSessionSearchResult ToEncapsulate);
    
    UFUNCTION(BlueprintCallable)
        int GetPing();

    UFUNCTION(BlueprintCallable)
        FString GetSessionIDStr() { return sessionResult.GetSessionIdStr(); }

    UFUNCTION(BlueprintCallable)
        bool IsSessionInfoValid() { return sessionResult.IsSessionInfoValid(); }

    UFUNCTION(BlueprintCallable)
        bool IsValid() { return sessionResult.IsValid(); }

    UFUNCTION(BlueprintCallable)
		void JoinSession();

    UFUNCTION(BlueprintCallable)
		bool TryTravelToCurrentSession();

    UPROPERTY(BlueprintAssignable)
		FCSOnJoinSessionComplete OnJoinGameSessionCompleteEvent;

private:
	USessionEncapsulation() {}
    FOnlineSessionSearchResult sessionResult;
    FOnlineSession session;

    FOnJoinSessionCompleteDelegate JoinSessionCompleteDelegate;
    FDelegateHandle JoinSessionCompleteDelegateHandle;
    void OnJoinSessionCompleted(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

    EOnJoinSessionStatus::type CastJoinResultToTrueResult(EOnJoinSessionCompleteResult::Type type);
};