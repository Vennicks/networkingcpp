// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OnlineSubsystemTypes.h"
#include "GameFramework/GameModeBase.h"
#include "SessionEncapsulation.h"
#include "Interfaces/OnlineSessionDelegates.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MenuMode.generated.h"

#define KEY_SESSION_NAME "SessionName"

//Find Sessions Delegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCSOnSessionEncapsulation, const TArray<USessionEncapsulation*>&, SessionResults, bool, Successful);

//Create Session Delegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnCreateSessionComplete, bool, Successful);

//Update Session Delegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnUpdateSessionComplete, bool, Successful);

//Start Session Delegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnStartSessionComplete, bool, Successful);

//End Session
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnEndSessionComplete, bool, Successful);

//Destroy Session
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCSOnDestroySessionComplete, bool, Successful);

USTRUCT(BlueprintType)
struct FSessionDefinition
{

	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
		bool AllowInvites = true;
	UPROPERTY(BlueprintReadWrite)
		bool JoinInGame = true;
	UPROPERTY(BlueprintReadWrite)
		bool AllowPlayerHost = false;
	UPROPERTY(BlueprintReadWrite)
		bool UseLan = false;
	UPROPERTY(BlueprintReadWrite)
		bool Advertise = true;
	UPROPERTY(BlueprintReadWrite)
		FString SessionName;
};

UCLASS()
class NETWORKINGCPP_API AMenuMode : public AGameModeBase
{
	GENERATED_BODY()


public:
	AMenuMode();

	//Create Session
	UFUNCTION(BlueprintCallable, Category = "Create Session")
		void CreateSession(FSessionDefinition NewSession);
	UPROPERTY(BlueprintAssignable, Category = "Create Session")
		FCSOnCreateSessionComplete OnCreateSessionCompleteEvent;

	//Destroy Session
	UFUNCTION(BlueprintCallable, Category = "Destroy Session")
		void DestroySession();
	UPROPERTY(BlueprintAssignable, Category = "Destroy Session")
		FCSOnDestroySessionComplete OnDestroySessionCompleteEvent;
	
	//Update Session
	UFUNCTION(BlueprintCallable, Category = "Update Session")
		void UpdateSession(FSessionDefinition NewDefinition);
	UPROPERTY(BlueprintAssignable, Category = "Update Session")
		FCSOnUpdateSessionComplete OnUpdateSessionCompleteEvent;

	//Start Session
	UFUNCTION(BlueprintCallable, Category = "Start Session")
		void StartSession();
	UPROPERTY(BlueprintAssignable, Category = "Start Session")
		FCSOnStartSessionComplete OnStartSessionCompleteEvent;

	//End Session
	UFUNCTION(BlueprintCallable, Category = "End Session")
		void EndSession();
	UPROPERTY(BlueprintAssignable, Category = "End Session")
		FCSOnEndSessionComplete OnEndSessionCompleteEvent;

	//Find Session
	UFUNCTION(BlueprintCallable, Category = "Find Session")
		void FindSessions(int32 MaxSearchResults, bool IsLANQuery);
	UPROPERTY(BlueprintAssignable, Category = "Find Session")
		FCSOnSessionEncapsulation OnFindSessionsCompleteEvent;

private:
	TSharedPtr<FOnlineSessionSearch> LastSessionSearch;
	TSharedPtr<FOnlineSessionSettings> LastSessionSettings;

	//Create Session
	void OnCreateSessionCompleted(FName SessionName, bool Successful);
	FOnCreateSessionCompleteDelegate  CreateSessionCompleteDelegate;
	FDelegateHandle CreateSessionCompleteDelegateHandle;
	
	//Destroy Session
	void OnDestroySessionCompleted(FName SessionName, bool Successful);
	FOnDestroySessionCompleteDelegate DestroySessionCompleteDelegate;
	FDelegateHandle DestroySessionCompleteDelegateHandle;

	//Update Session
	void OnUpdateSessionCompleted(FName SessionName, bool Successful);
	FOnUpdateSessionCompleteDelegate UpdateSessionCompleteDelegate;
	FDelegateHandle UpdateSessionCompleteDelegateHandle;
	
	//Start Session
	void OnStartSessionCompleted(FName SessionName, bool Successful);
	FOnStartSessionCompleteDelegate StartSessionCompleteDelegate;
	FDelegateHandle StartSessionCompleteDelegateHandle;

	//End Session
	void OnEndSessionCompleted(FName SessionName, bool Successful);
	FOnEndSessionCompleteDelegate EndSessionCompleteDelegate;
	FDelegateHandle EndSessionCompleteDelegateHandle;

	//Find Session
	void OnFindSessionsCompleted(bool Successful);
	FOnFindSessionsCompleteDelegate FindSessionsCompleteDelegate;
	FDelegateHandle FindSessionsCompleteDelegateHandle;
};
